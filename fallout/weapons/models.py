from django.db import models


class Weapon(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class WeaponProperty(models.Model):
    property_key = models.CharField(max_length=255)
    property_value = models.CharField(max_length=255)
    value_type = models.CharField(max_length=255, default='str')
    weapon_id = models.ForeignKey(
        'Weapon',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.property_value


class WeaponSlot(models.Model):
    slot_id = models.ForeignKey(
        'mods.Slot',
        on_delete=models.CASCADE,
    )
    weapon_id = models.ForeignKey(
        'Weapon',
        on_delete=models.CASCADE,
    )
