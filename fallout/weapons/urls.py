from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:weapon_id>/', views.detail, name='weapon_detail'),
    path('assemble/', views.assemble, name='weapon_assemble')
]
