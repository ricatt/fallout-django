# Generated by Django 2.2.1 on 2019-05-16 17:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mods', '0001_initial'),
        ('weapons', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WeaponSlot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slot_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mods.Slot')),
                ('weapon_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='weapons.Weapon')),
            ],
        ),
    ]
