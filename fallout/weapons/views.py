from django.shortcuts import render
from django.http import HttpResponse

from .models import Weapon
from fallout.common import WeaponAssembler
from mods.models import Mod

def index(request):
    weapons = Weapon.objects.all()
    context = {'weapons': weapons}
    return render(request, 'weapons/index.html', context)


def detail(request, weapon_id):
    weapon = Weapon.objects.get(pk=weapon_id)
    context = {'weapon': weapon}
    return render(request, 'weapons/detail.html', context)

def assemble(request):
    weapon_id = request.GET.get('weapon')
    print(weapon_id)
    mods_id = request.GET.get('mods')

    if mods_id is not None:
        mods = Mod.objects.filter(pk__in=mods_id.split(','))
    else:
        mods = []
    return HttpResponse(mods)
    # wa = WeaponAssembler()

    # weapon = Weapon.query.filter_by(id=weapon_id).first()
    # wa.set_weapon(weapon.flat_result())
    # for slot in weapon.slots:
    #     wa.add_slot(slot.label)

    # for mod in mods:
    #     wa.add_mod(mod.to_array())

    # result = wa.assemble()
    # return jsonify(result)