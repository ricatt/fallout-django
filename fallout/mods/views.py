from django.shortcuts import render
from django.http import HttpResponse

from .models import Mod


def index(request):
    mods = Mod.objects.all()
    context = {'mods': mods}
    return render(request, 'mods/index.html', context)


def detail(request, mod_id):
    mod = Mod.objects.get(pk=mod_id)
    context = {'mod': mod}
    return render(request, 'mods/detail.html', context)
