from django.db import models


class Mod(models.Model):
    name = models.CharField(max_length=255)
    weapon_id = models.ForeignKey(
        'weapons.Weapon',
        on_delete=models.CASCADE,
    )
    slot_id = models.ForeignKey(
        'Slot',
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return self.name


class ModProperty(models.Model):
    property_key = models.CharField(max_length=255)
    property_value = models.CharField(max_length=255)
    value_type = models.CharField(max_length=255, default='str')
    mod_id = models.ForeignKey(
        'Mod',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.property_value


class Slot(models.Model):
    label = models.CharField(max_length=255, unique=True)
    category = models.CharField(max_length=255)

    def __str__(self):
        return self.label
