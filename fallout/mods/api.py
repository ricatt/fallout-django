from django.http import JsonResponse
from django.core import serializers
from .models import Mod

def list(request):
    mods = Mod.objects.all()
    return JsonResponse(serializers.serialize('json', mods), safe=False)