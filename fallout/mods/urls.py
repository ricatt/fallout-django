from django.urls import path

from . import views
from . import api

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:mod_id>/', views.detail, name='mod_detail'),

    path('api', api.list, name="api_list")
]
