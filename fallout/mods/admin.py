from django.contrib import admin

from .models import Mod, Slot


admin.site.register(Mod)
admin.site.register(Slot)
